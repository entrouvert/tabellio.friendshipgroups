from five import grok
from zope import schema
from zope.interface import implements

from plone.directives import form, dexterity
from plone.dexterity.content import Container
from plone.app.textfield import RichText

from tabellio.friendshipgroups import MessageFactory as _

class IGroup(form.Schema):
    memorandum = RichText(title=_(u'Memorandum'), required=False)
    minutes = RichText(title=_(u'Minutes'), required=False)

class Group(Container):
    implements(IGroup)
