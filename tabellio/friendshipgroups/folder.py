from Products.Five import BrowserView
from zope.interface import Interface


class IFriendshipGroupsFolder(Interface):
    pass

class FriendshipGroupsFolder(BrowserView):
    pass

